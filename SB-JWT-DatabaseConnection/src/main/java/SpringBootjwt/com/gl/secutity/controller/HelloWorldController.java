package SpringBootjwt.com.gl.secutity.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import SpringBootjwt.com.gl.secutity.model.UserDTO;

@RestController
@CrossOrigin()
public class HelloWorldController {

	@RequestMapping({ "/hello" })
	public String hello() {
		return "Hello World";
	}
	
	@RequestMapping("/getForm")
	public ModelAndView getregisterPage() {
		return new ModelAndView("RegisterPage","registerData",new UserDTO());
	}
	@RequestMapping("/getLoginForm")
	public ModelAndView getLoginPage() {
		return new ModelAndView("LoginPage","obj",new UserDTO());
	}

}
