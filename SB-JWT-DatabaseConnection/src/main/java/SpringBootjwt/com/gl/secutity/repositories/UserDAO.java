package SpringBootjwt.com.gl.secutity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import SpringBootjwt.com.gl.secutity.model.DAOUser;


@Repository
public interface UserDAO extends JpaRepository<DAOUser,Integer>
{
    DAOUser findByUsername(String username);
}


